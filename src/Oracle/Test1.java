package Oracle;

import java.util.Scanner;

public class Test1 {
	
	public static void main(String[] args) {
		
		
		/*
		 
		 
		 SELECT 선택하다. 컬럼(행)/로우(열)
		       * (아스타)
		   FROM EMPLOYEE     - EMPLOYEE 테이블을 보여줘라.
		 ORDER BY PRICE	     - PRICE로 내림차순,
		 ORDER BY PRICE DESC - PRICE로 오름차순,
		 ORDER BY YEAR,MONTH,DAY - 가장오래된순으로 나온다.
		 
		 
		 SELECT 
		        COUNT(*)     - 갯수를 세고 싶을때 쓴다.
		   FROM EMPLOYEE     - EMPLOYEE 의 총 컬럼이 몇개 인가?
		   
		   
		 -- 직원 테이블에서 주민번호 8번째 자리를 조회하여 1이면 남, 2이면 여로 결과를 조회하고
		 -- 성별별 급여 평균(정수처리), 급여 합계, 인원수를 조회한 뒤 인원수로 내림차순 정렬하기
		 -- 조건(WHERE)이 따로 없음.
		 SELECT
		        DECODE(SUBSTR(EMP_NO, 8, 1),1,'남', '2','여')성별
		      , FLOOR(AVG(SALARY))
		      , SUM(SALARY)
		      , COUNT(*)        
		   FROM EMPLOYEE
		  GROUP BY DECODE(SUBSTR(EMP_NO, 8, 1),1,'남', '2','여')
		  ORDER BY COUNT(*) DESC;
 		
 		//
 		DECODE "JAVA 삼항연산자 같은아이"
 		FLOOR(내림처리)
 		SUM(합계)
 		COUNT(전체 컬럼수)
 		GROUP BY 로 DECODE를 묶어준다. 남과 여
 		ORDER BY COUNT를 DESC 내림처리 해준다. 
 		//
		 
		 
		 
		-- 부서 별 그룹의 급여 합계 중에 900만원을 초과하는 부서 코드와 급여 합계를 조회해보자.
		SELECT
		       DEPT_CODE
		     , SUM(SALARY)  
		  FROM EMPLOYEE
		 GROUP BY DEPT_CODE
		 HAVING SUM(SALARY) > 9000000; 
		 
		 //
		 DEPT_CODE 부서코드
		 SUM(SALARY) 월급 합계,
		 GROUP BY DEPT_CODE 부서 묶음처리,
		 HAVING GROUP BY의 WHERE절 같은 느낌으로 처리한다.
		 // 
		 

		 
		 */
		
		
		
		
		
		
		
		
		
		
		
	}
	
	

}

